#!/bin/bash
set -e
if [ "$1" = 'emulator' ]; then
  source `dirname $0`/env.sh

  echo no | android create avd --force -n $EMULATOR_NAME -t $EMULATOR_TARGET --abi $EMULATOR_ABI
  echo "hw.keyboard=yes" >> ~/.android/avd/$EMULATOR_NAME.avd/config.ini
  echo "Enabled hardware keyboard"
  cat ~/.android/avd/$EMULATOR_NAME.avd/config.ini
  adb logcat &
  exec emulator-arm -avd $EMULATOR_NAME -no-audio -no-window
fi

exec "$@"
