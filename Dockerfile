FROM debian:7
MAINTAINER Olivier Tabone <olivier.tabone@ripplemotion.fr>

RUN set -x && \
   dpkg --add-architecture i386 && \
   apt-get update && \
   apt-get install -y --force-yes expect git wget libc6-i386 lib32stdc++6 lib32gcc1 lib32ncurses5 lib32z1 && \
   apt-get clean && rm -rf /var/lib/apt

RUN set -x && \
   cd /opt && \
   wget --header "Cookie: oraclelicense=accept-securebackup-cookie" --progress=dot:mega http://download.oracle.com/otn-pub/java/jdk/7u25-b15/jdk-7u25-linux-x64.tar.gz -O jdk-7u25-linux-x64.tar.gz && \
   tar -xzf jdk-7u25-linux-x64.tar.gz && \
   ln -s jdk1.7.0_25 jdk && \
   rm -rf jdk-7u25-linux-x64.tar.gz

ENV JAVA_HOME "/opt/jdk"
ENV PATH "$JAVA_HOME/bin:$PATH"

RUN set -x && \
   cd /opt/ && \
   wget --output-document=android-sdk.tgz --progress=dot:mega http://dl.google.com/android/android-sdk_r24.1.2-linux.tgz && \
   tar -xzf android-sdk.tgz && \
   rm -f android-sdk.tgz && \
   chown -R root.root android-sdk-linux

ENV ANDROID_HOME /opt/android-sdk-linux
ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools

ADD android-update.exp /tmp/

RUN set -x \
   && expect -f /tmp/android-update.exp platform-tools,tools,build-tools-22,android-22,extra-android-support,extra-android-m2repository,extra-google-m2repository,sys-img-armeabi-v7a-android-22
   
RUN mkdir -p /docker-entrypoint-emulator.d
COPY *.sh /docker-entrypoint-emulator.d/

ENV ANDROID_EMULATOR_FORCE_32BIT true


ENTRYPOINT ["/docker-entrypoint-emulator.d/init-emulator.sh"]
CMD ["emulator"]

EXPOSE 5555


